import RobotStore from '../stores/RobotStore'
import Robot from './Robot'
import React from 'react'

class RobotForm extends React.Component{
    constructor(props){
		super(props)
		this.state = {
			name:'',
			type:'',
			mass:''
		}
	}
		
    handleChange=(event)=>{
       this.setState({
           [event.target.name]:event.target.value
       })
    }
    handleAdd=() => this.props.onAdd({
	    name:this.state.name,
		type:this.state.type,
	    mass:this.state.mass
	})
   
    	render() {
		return (
			<div>
			      <input type="text" id="name" name="name" onChange={this.handleChange}/>
			      <input type="text" id="type" name="type" onChange={this.handleChange}/>
			      <input type="text" id="mass" name="mass" onChange={this.handleChange}/>
			      <input type="button" value="add" onClick={this.handleAdd}/>
			
			</div>
		)
	}
    
}
export default RobotForm;